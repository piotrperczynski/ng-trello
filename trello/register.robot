*** Settings ***
Suite Setup       Run Keywords  Start Tests
Suite Teardown    Run Keywords  End Tests
Resource          ../base.robot
Resource          ../Artifacts/trello_keywords.robot
Resource          ../Artifacts/trello_variables.robot

*** Test Cases ***

Register - test case 4
    [Tags]  Done
    Wait till visible   //*[contains(text(),'Trello lets you work more collaboratively and get more done.')]
    wait and click  //a[@class='btn btn-sm bg-white font-weight-bold']
    wait till visible  //div[@id='signup-password']//h1[contains(text(),'Create a Trello Account')]
    input text  //input[@id='email']    a@a
    wait and click  //input[@id='signup']
    wait till visible  //input[@id='email' and @value='a@a']
    wait till visible  //input[@id='signup' and @disabled]
    input text  //input[@id='name']     a a
    input text  //input[@id='password']     1
    wait and click  //input[@id='signup']
    wait till visible  //p[@class='error-message' and contains(text(),'Invalid email')]
    wait and click  //input[@id='name']
    wait till visible  //div[@id='email-error' and contains(text(),'look like a can receive email')]
    input text  //input[@id='email']    ${email}
    wait and click  //input[@id='signup']
    wait till visible  //div[contains(text(),'Hey, that email is already in use by another Atlas')]
    ${random}=   Generate Random String  5   [LETTERS])
    input text  //input[@id='email']    testuser${random}@jmail.ovh
    wait and click  //input[@id='signup']
    wait till visible  //div[contains(text(),'Hey, that email is already in use by another Atlas')]
    wait till visible  //p[@class='error-message' and contains(text(),'Password must be at least 8 characters')]
    input text  //input[@id='password']     1234567890
    wait and click  //input[@id='signup']
    wait till visible  //span[contains(text(),'You can organize just about anything with a Trello')]
