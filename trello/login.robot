*** Settings ***
Suite Setup       Run Keywords  Start Tests
Suite Teardown    Run Keywords  End Tests
Resource          ../base.robot
Resource          ../Artifacts/trello_keywords.robot
Resource          ../Artifacts/trello_variables.robot

*** Test Cases ***


Login - test case 7
    [Tags]  Done
    Wait till visible   //*[contains(text(),'Trello lets you work more collaboratively and get more done.')]
    wait and click  //a[@class='btn btn-sm btn-link text-white']
    wait till visible   //input[@id='user']
    wait till visible   //input[@id='password']
    wait till visible   //h1[contains(text(),'Log in to Trello')]
    wait and click  //input[@id='login']
    wait till visible  //p[@class='error-message' and contains(text(),'Missing email')]
    input text  //input[@id='password']     qwerty
    wait and click  //input[@id='login']
    wait till visible  //p[@class='error-message' and contains(text(),'Missing email')]
    input text  //input[@id='user']     asdasd@sadsad
    wait and click  //input[@id='login']
    wait till visible  //p[@class='error-message' and contains(text(),'There isn')]
    input text  //input[@id='user']     asdasd
    wait and click  //input[@id='login']
    wait till visible  //p[@class='error-message' and contains(text(),'perhaps you normally log in with Google or SSO?')]
    input text  //input[@id='user']     trellotestzxzx@jmail.ovh
    wait until element is not visible  //input[@id='password']
    wait till visible  //input[@id='login' and @value='Log in with Atlassian']
    wait and click  //input[@id='login' and @value='Log in with Atlassian']
    wait till visible  //input[@id='username' and @value='trellotestzxzx@jmail.ovh']
    wait and click  //span[contains(text(),'Continue')]
    wait till visible  //input[@id='password']
    input text  //input[@id='password']     testingtrello
    wait and click  //span[contains(text(),'Log in')]
    wait till visible  //div[@class='board-tile-details-name']//div[contains(text(),'Welcome to Trello!')]
