1. Install Pycharm (community edition is enough) https://www.jetbrains.com/pycharm/download/#section=windows
2. Install Python version, for example 3.7.5
https://www.python.org/downloads/
3. Open cmd and run
'pip install robotframework' and 'pip install robotframework-seleniumlibrary'
4. Make sure pythons is added to paths:
my computer -> right click -> properties -> advanced system settings -> environment variables ->
in system variables select PATH and edit -> make sure these two lines are present:
C:\Users\username\AppData\Local\Programs\Python\Python37-32\
C:\Users\username\AppData\Local\Programs\Python\Python37-32\scripts
5. Import project to Pycharm from https://piotrperczynski@bitbucket.org/piotrperczynski/ng-trello
6. In Pycharm open
File -> settings -> Project:<project name> -> project interpreter and choose version of python you installed
in table with packages there should be robotframework and robotframework-seleniumlibrary visible
7. In Pycharm open
File -> settings -> Plugins
and install IntelliBot

From now on you can run test cases using Pycharm Terminal:
robot -T -i done trello/login.robot
robot -T -i done trello/register.robot
or
run login.bat and register.bat files from project directory.

Html report with timestamp will be generated.