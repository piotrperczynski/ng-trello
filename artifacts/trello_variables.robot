*** Variables ***
${URL}            https://trello.com/en/home
${BROWSER}        Chrome
${username}       Test User
${email}          trellotestzxzx@jmail.ovh
${password}       testingtrello
${wrongemail}     @.@.

&{Global}              Button_NewInitiative=//div[contains(@class, 'newInitiative')]
...                    Button_Next=//td[@class='nextButton']
...                    Button_AsIOL=//nobr[contains(text(),'as IOL')]
...                    Button_AsMSM=//nobr[contains(text(),'as MSM')]
...                    Button_CloseWindow=//div[@class='closeWindowButton']
...                    LoadingEnd=(//div[contains(@style,'visibility: hidden') and @class='modalMask'])
...                    ReportsList=//td[contains(text(),'Reports')]
...                    SetupTasksList=//td[contains(text(),'Setup Tasks')]
...                    ChangeAlertsList=//td[contains(text(),'Change Alerts')]
