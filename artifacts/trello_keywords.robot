*** Settings ***
Resource          ../base.robot
Resource          ../Artifacts/trello_variables.robot

*** Keywords ***
Start Tests
    # Suite Setup
    set selenium timeout  15 seconds
    ${chrome options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Create Webdriver    ${BROWSER}    chrome_options=${chrome options}
    go to    ${URL}
    Maximize Browser Window


End Tests
    # Suite Teardown
    Close All Browsers

Wait and Click
    [Arguments]    ${arg}
    Wait Until Page Contains Element  ${arg}  timeout=10s
    Click Element    ${arg}

Wait till visible
    [Arguments]    ${arg}
    wait until element is visible   ${arg}  timeout=10s
